FROM mhart/alpine-node:7

COPY . .
RUN apk add --no-cache \
  libc6-compat \
  && npm install --only=production

ENTRYPOINT ["node", "bin/relay"]
