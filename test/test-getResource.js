const chai = require("chai");
const should = chai.should();

const Relay = require("../Relay");

const defaultProjectID = "dummy-project";

class FakeRelay extends Relay {

  constructor(opts) {
    super(Object.assign({}, opts, {
      skipClient: true,
      projectID: defaultProjectID,
    }));
  }

  getInstanceID() {
    return Promise.resolve("dummy-instance");
  }

  getZone() {
    return Promise.resolve("dummy-zone");
  }

}

describe("Relay#getResource", function() {

  function isGlobalResource(res) {
    res.should.be.an.object;
    res.should.have.property("type", "global");
    res.should.have.property("labels");
    res.labels.should.have.property("project_id", defaultProjectID);
  }

  function isGCEInstance(res) {
    res.should.be.an.object;
    res.should.have.property("type", "gce_instance");
    res.should.have.property("labels");
    res.labels.should.have.property("instance_id", "dummy-instance");
    res.labels.should.have.property("zone", "dummy-zone");
    res.labels.should.not.have.property("project_id");
  }

  it("should use 'global' by default", function() {
    return Promise
      .resolve(new FakeRelay().getResource())
      .then(isGlobalResource);
  });

  it("should use 'global' if type isn't known", function() {
    return Promise
      .resolve(new FakeRelay({ type: "__bogus__" }).getResource())
      .then(isGlobalResource);
  });

  it("should work with 'global' type", function() {
    return Promise
      .resolve(new FakeRelay({ type: "global" }).getResource())
      .then(isGlobalResource);
  });

  it("should work with 'gce_instance' type", function() {
    return Promise
      .resolve(new FakeRelay({ type: "gce_instance" }).getResource())
      .then(isGCEInstance);
  });

});
