if [[ "$CI_COMMIT_REF_NAME" == "master" ]] || [[ "$(git log -1 --pretty=%B)" == *"[deploy pages]"* ]]; then
  echo "Building pages.";
else
  echo "Skipping pages build.  Include [deploy pages] in the commit message to build pages when not on master.";
  cp -r last_master/ public/;
  exit 1;
fi
