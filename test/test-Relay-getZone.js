const chai = require("chai");
const should = chai.should();

const nock = require("nock");
const Relay = require("../Relay");

describe("Relay#getZone", function() {

  let relay, scope, lastFlavor;

  beforeEach(function() {
    lastFlavor = null;
    relay = new Relay({ skipClient: true });
    scope = nock("http://metadata.google.internal", {
        reqheaders: {
          "Metadata-Flavor": (flavor) => {
            lastFlavor = flavor;
            return true;
          },
        },
      })
      .get("/computeMetadata/v1/instance/zone")
      .reply(200, "projects/sample-project-id/zones/dummy-zone");
  });

  it("should connect to 'metadata.google.internal'", function() {
    return relay
      .getZone()
      .then(data => {
        scope.isDone().should.equal(true);
      });
  });

  it("should provide 'Metadata-Flavor' header", function() {
    return relay
      .getZone()
      .then(data => {
        lastFlavor.should.equal("Google");
      });
  });

  it("should relay data", function() {
    return relay
      .getZone()
      .then(data => {
        data.should.equal("dummy-zone");
      });
  });

});
