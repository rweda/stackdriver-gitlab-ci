const chai = require("chai");
const should = chai.should();

const Relay = require("../Relay");

const withBuild = `
# HELP ci_docker_machines The total number of machines created.
# TYPE ci_docker_machines counter
ci_docker_machines{type="created"} 0
ci_docker_machines{type="removed"} 0
ci_docker_machines{type="used"} 0
# HELP ci_runner_builds The current number of running builds.
# TYPE ci_runner_builds gauge
ci_runner_builds{stage="prepare_script",state="running"} 20
# HELP ci_ssh_docker_machines The total number of machines created.
# TYPE ci_ssh_docker_machines counter
ci_ssh_docker_machines{type="created"} 0
ci_ssh_docker_machines{type="removed"} 0
ci_ssh_docker_machines{type="used"} 0
`;

const missingBuilds = `
# HELP ci_docker_machines The total number of machines created.
# TYPE ci_docker_machines counter
ci_docker_machines{type="created"} 0
ci_docker_machines{type="removed"} 0
ci_docker_machines{type="used"} 0
# HELP ci_ssh_docker_machines The total number of machines created.
# TYPE ci_ssh_docker_machines counter
ci_ssh_docker_machines{type="created"} 0
ci_ssh_docker_machines{type="removed"} 0
ci_ssh_docker_machines{type="used"} 0
`;

const multipleBuilds = `
ci_runner_builds{stage="prepare_script",state="running"} 15
ci_runner_builds{stage="random-stage",state="running"} 15
`

describe("Relay#parseMetrics", function() {

  let relay;

  before(function() {
    relay = new Relay({ skipClient: true });
  });

  it("should parse metrics out of a Prometheus line", function() {
    relay.parseMetrics(`ci_runner_builds{stage="prepare_script",state="running"} 1`).should.equal(1);
  });

  it("should default to 0 if not given any data", function() {
    relay.parseMetrics("").should.equal(0);
  })

  it("should parse metrics out of a Prometheus file", function() {
    relay.parseMetrics(withBuild).should.equal(20);
  });

  it("should default to 0 if the Prometheus data doesn't include 'ci_runner_builds'", function() {
    relay.parseMetrics(missingBuilds).should.equal(0);
  });

  it("should parse builds from multiple stages", function() {
    relay.parseMetrics(multipleBuilds).should.equal(30);
  });

});
