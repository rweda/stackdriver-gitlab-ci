const socket = require("socket.io-client")("http://localhost:8888");

console.log("Fake CI job started.");

socket.on("stop", function() {
  console.log("Fake CI job stopping.");
  socket.emit("stopped");
  process.exit();
});
