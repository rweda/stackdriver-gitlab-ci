/**
 * Integration test for `stackdriver-gitlab-ci`.
 *
 * This test is partially disabled, as running `gitlab-runner exec` doesn't officially increase the count of running
 * jobs.  However, we still start up the Relay and ensure that it outputs some data, which ensures that it got some
 * response from GitLab and continues to run for a period of time.
 * To re-enable the test, a GitLab instance needs to be setup or mocked, and jobs will probably need to be sent over the
 * official channels.
 *
 * Steps 2-5 are currently disabled.
 *
 * 1) Check that no jobs are currently running
 * 2) Start fake CI jobs that connect back to this script.
 * 3) Make sure that the Relay server counts the right number of jobs.
 * 4) Stop the fake jobs.
 * 5) Make sure the Relay server returns to counting `0` jobs.
*/

const chai = require("chai");
const should = chai.should();

const chalk = require("chalk");
const Promise = require("bluebird");
const express = require("express");
const fs = require("fs-extra");
const path = require("path");
const exec = require("child-process-promise").exec;
const spawn = require("child-process-promise").spawn;
const EventEmitter = require("events");
const thread = require("threads").spawn;

const relayInterval = 100;
const jobRunnerPort = 8888;
const count_file = path.resolve(`${__dirname}/../integration-data/last-datapoint.json`);
const initial_wait = 60 * 1000;

/**
 * Starts new fake CI jobs.
*/
class JobRunner extends EventEmitter {

  constructor() {
    super();
    this.running = [];
    this.responded = [];
    this.stopped = [];
    this.io = require("socket.io")(jobRunnerPort);
    this.io.on("connection", socket => {
      this.responded.push(socket);
      this.emit("started");
      socket.on("stopped", () => {
        this.stopped.push(socket);
        this.emit("stopped");
      });
    });
  }

  /**
   * Starts one or more fake CI jobs.
   * @param {Integer} [count] the number of CI jobs to start.  Defaults to `1`.
   * @return {Promise} resolves when all jobs have been started.
  */
  start(count) {
    if(!count) { count = 1; }
    let wait = Promise.resolve();
    for(let i = 0; i<count; i++) {
      wait = wait.then(() => {
        let job = spawn("gitlab-runner", ["exec", "shell", "fake-ci-job"], {
          cwd: path.resolve(`${__dirname}/../../`),
        });
        if(i === 0) {
          job.childProcess.stdout.pipe(process.stdout);
          job.childProcess.stderr.pipe(process.stderr);
        }
        this.running.push(job);
      }).then(() => Promise.delay(2000));
    }
    return new Promise((resolve, reject) => {
      this.on("started", () => {
        console.log(`${this.responded.length} fake CI jobs have responded.  ${this.running.length} running, ${count} to be created.`);
        if(this.running.length > this.responded.length || count > this.running.length) { return; }
        resolve();
      });
    });
  }

  /**
   * Stops all fake CI jobs.
  */
  stop() {
    this.io.emit("stop");
    return new Promise((resolve, reject) => {
      this.on("stopped", () => {
        if(this.running.length > this.stopped.length) { return; }
        resolve();
      });
    });
  }

}

/**
 * Reads the last data written by the Relay system.
 * @return {Promise<Object>} `{val, time}` the last value and time recorded.
*/
function relayData(attempts) {
  if(!attempts) { attempts = 0; }
  t = thread((input, done) => {
    require("fs").readFile(input.file, "utf8", (err, data) => {
      if(err) { throw err; }
      done(data);
    });
  });
  return t
    .send({ file: count_file })
    .promise()
    .then(JSON.parse)
    .catch(err => {
      if(attempts < 5) {
        console.log(`File read failed (${attempts} attempts).  Retrying.`);
        return relayData(attempts + 1);
      }
      console.error("Multiple attempts to read Relay output file have failed, possibly due to SEGFAULT.  Aborting.");
      throw err;
    });
}

function waitForData() {
  let start = Date.now();
  console.log(Array(51).join("--"));
  function attempt() {
    console.log("-");
    return relayData()
      .then(() => {
        console.log(Array(51).join("--"));
        console.log(`Relay started writing output after ${Date.now() - start}ms.`);
      })
      .catch(err => {
        if(Date.now() - start < initial_wait) {
          process.stdout.write("=");
          return Promise.delay(initial_wait / 50).then(attempt);
        }
        console.log("");
        throw err;
      });
  }
  return attempt();
}

function printDockerLogs() {
  console.log("Docker logs:");
  s = spawn("docker", ["logs", "relay"]);
  s.childProcess.stdout.pipe(process.stdout);
  s.childProcess.stderr.pipe(process.stderr);
  return s;
}

let testJobs = 15;
let runner = new JobRunner();

let _lastTime = null;

function logStep(msg) {
  console.log(chalk.blue("+") + "  " + chalk.yellow(msg));
}

logStep("Waiting for Relay to start.");

waitForData()
  .then(() => {
    logStep("Relay has started writing data.  Reading initial data.");
    return relayData();
  })
  .then(({ val, time }) => {
    logStep(`Retrieved initial data.  Ensuring that data is updated every ${relayInterval}ms.`);
    _lastTime = time;
    val.should.equal(0, "no machines should be running at the start");
  })
  .then(() => Promise.delay(relayInterval))
  .then(relayData)
  .then(({ val, time }) => time.should.not.equal(_lastTime, `Relay should update values after ${relayInterval}ms`))
  /*
  .then(() => {
    logStep(`Starting ${testJobs} fake CI jobs.`);
    return runner.start(testJobs);
  })
  .then(() => Promise.delay(relayInterval))
  .then(() => logStep("Started fake CI jobs.  Ensuring that relay can read the jobs."))
  .then(relayData)
  .then(({ val, time }) => _lastTime = time)
  .then(() => Promise.delay(relayInterval))
  .then(relayData)
  .then(({ val, time }) => {
    time.should.not.equal(_lastTime, `Relay should still be running.`);
    val.should.equal(testJobs);
    logStep(`Relay confirmed that ${testJobs} jobs are running.  Stopping jobs.`);
  })
  .then(() => runner.stop())
  .then(() => Promise.delay(relayInterval))
  .then(() => logStep("Jobs have been stopped.  Ensuring that Relay resets to 0."))
  .then(relayData)
  .then(({ val, time }) => {
    val.should.equal(0, "Relay should reset back to 0.");
  })*/
  .then(() => logStep("Integration test has passed."))
  .catch(err => {
    console.log("Integration test failed with error:");
    console.log(err.stack);
    printDockerLogs().then(() => process.exit(1));
  })
  .then(() => process.exit(0));
