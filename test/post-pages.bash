if [[ "$CI_COMMIT_REF_NAME" == "master" ]]; then
  echo "On master.  Saving 'public/' as 'last_master/'.";
  rm -rf last_master/;
  cp -r public/ last_master/;
fi
