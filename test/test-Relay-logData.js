const chai = require("chai");
const should = chai.should();

const path = require("path");
const random = require("lodash.random");
const Relay = require("../Relay");


let opts;
try {
  opts = require("./opts");
  opts.authFile = path.join(__dirname, opts.authFile);
}
catch (e) { opts = {}; }

if(process.env.TEST_PROJECT) { opts.projectID = process.env.TEST_PROJECT; }
if(process.env.AUTH_FILE) { opts.authFile = process.env.AUTH_FILE; }

describe("Relay#logData", function() {

  let relay, time, val;

  before(function() {
    time = Date.now();
    val = random(0, 900);
    relay = new Relay(opts);
    return relay.logData(val, time);
  });

  it("submits a time series", function() {
    this.timeout(4 * 1000);
    return relay.client
      .listTimeSeries({
        name: relay.client.projectPath(opts.projectID),
        filter: `metric.type="${relay.metricType}"`,
        interval: {
          startTime: { seconds: (time / 1000) - (60 * 2) }, // start 2 minutes before the submitted time
          endTime: { seconds: (time / 1000) + (60 * 1) }, // end 1 minute after the submitted time
        }
      })
      .then(res => {
        res.should.be.an.array;
        res.length.should.equal(1, `outer array should contain 1 entry`);
        const series = res[0];
        series
          .some(data => data.points.some(point => parseInt(point.value.int64Value, 10) === val))
          .should.equal(true);
      });
  });

});
