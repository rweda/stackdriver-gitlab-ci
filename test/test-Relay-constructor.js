const chai = require("chai");
const should = chai.should();

const Relay = require("../Relay");

describe("Relay#constructor", function() {

  describe("opts", function() {

    it("should accept a URL", function() {
      new Relay({ url: "test", skipClient: true }).url.should.equal("test");
    });

  });

});
