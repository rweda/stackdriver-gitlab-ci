const chai = require("chai");
const should = chai.should();

const nock = require("nock");
const Relay = require("../Relay");

class FakeRelay extends Relay {

  constructor(opts) {
    super(opts);
    this._lastVal = null;
  }

  logData(val) {
    this._lastVal = val;
  }

}

describe("Relay#queryMetrics", function() {

  let relay = new FakeRelay({ url: "http://localhost:9999/", skipClient: true });

  it("should query localhost", function() {
    let scope = nock("http://localhost:9999").get("/").reply(200, "ok");
    return relay.queryMetrics().then(() => scope.isDone().should.equal(true));
  });

  it("should parse responses", function() {
    let scope = nock("http://localhost:9999").get("/")
      .reply(200, `ci_runner_builds{stage="prepare_script",state="running"} 2`);
    return relay.queryMetrics().then(() => relay._lastVal.should.equal(2));
  });

});
