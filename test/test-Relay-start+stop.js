const chai = require("chai");
const should = chai.should();

const Promise = require("bluebird");
const Relay = require("../Relay");

class FakeRelay extends Relay {

  constructor(opts) {
    super(opts);
    this._queries = 0;
  }

  queryMetrics() { this._queries++; }

}

describe("Relay#start and Relay#stop", function() {

  it("ignores an immediate 'stop'", function() {
    new FakeRelay({ skipClient: true }).stop();
  });

  it("should start and stop", function() {

    let relay = new FakeRelay({ skipClient: true, interval: 100 });
    relay._queries.should.equal(0);
    relay.start();
    return Promise
      .delay(10)
      .then(() => relay._queries.should.equal(1, "query should run immediately"))
      .delay(100)
      .then(() => relay._queries.should.equal(2, "query should have been run once"))
      .then(() => Promise.delay(100))
      .then(() => {
        relay._queries.should.equal(3, "query should keep running");
        relay.stop();
        return Promise.delay(200);
      })
      .then(() => relay._queries.should.equal(3, "query should stop running"));
  });

});
