const chai = require("chai");
const should = chai.should();

const nock = require("nock");
const Relay = require("../Relay");

describe("Relay#getInstanceID", function() {

  let relay, scope, lastFlavor;

  beforeEach(function() {
    lastFlavor = null;
    relay = new Relay({ skipClient: true });
    scope = nock("http://metadata.google.internal", {
        reqheaders: {
          "Metadata-Flavor": (flavor) => {
            lastFlavor = flavor;
            return true;
          },
        },
      })
      .get("/computeMetadata/v1/instance/id")
      .reply(200, "dummy-id");
  });

  it("should connect to 'metadata.google.internal'", function() {
    return relay
      .getInstanceID()
      .then(data => {
        scope.isDone().should.equal(true);
      });
  });

  it("should provide 'Metadata-Flavor' header", function() {
    return relay
      .getInstanceID()
      .then(data => {
        lastFlavor.should.equal("Google");
      });
  });

  it("should relay data", function() {
    return relay
      .getInstanceID()
      .then(data => {
        data.should.equal("dummy-id");
      });
  });

});
