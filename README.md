# Stackdriver Monitoring for GitLab CI

Relays GitLab CI Runnner status to Google Stackdriver.

This would allow servers running GitLab CI Runner on Google's Cloud Compute Engine to autoscale according to the
number of jobs each server is running, using a standardized scaling technique for the Google Cloud Platform.

This relay runs in the background, requesting and parsing the [Prometheus metrics][runner-monitoring] that are built in
to the GitLab CI Runner, and updating a [Stackdriver][] metric with the number of running jobs.

The relay is written in JavaScript for NodeJS, and can be run either on a machine with NodeJS installed, or installed as
a Docker image with all dependencies included.

## Programmatic Usage

The relay can be installed as a NodeJS library.

```bash
npm install --save stackdriver-gitlab-ci-relay
```

JavaScript API docs: https://rweda.gitlab.io/stackdriver-gitlab-ci/docs/

## Command-Line Usage

For most use cases, the relay can be started as a command-line utility.

### Installation

#### Docker Image

```bash
docker pull registry.gitlab.com/rweda/stackdriver-gitlab-ci:latest
```

##### Example Usage (Docker Install)

```bash
LOCAL_IP=`ip -4 addr show eth0 | grep inet | awk '{print $2}' | sed 's/\/[0-9]*//'`

docker run --name ci-relay --add-host="runner:$LOCAL_IP" -v "/root/ci-relay:/var/ci-relay" -d \
  registry.gitlab.com/rweda/stackdriver-gitlab-ci:latest --url "http://runner:9999/metrics"
```

- `-d`
  starts the Docker container running in the background.
- `--name`
  gives the Docker container a name, so you can use `docker stop ci-relay` when finished.
- `--add-host` and `--url`
  set up a connection to the GitLab CI Runner to collect metrics.  `--add-host` creates an entry in `/etc/hosts`
  aliasing the host's local IP address as `runner`.
- `-v`
  connects the `/var/ci-relay` directory inside the Relay's docker image to a directory on the host machine
  (`/root/ci-relay`) which will be used to pass Stackdriver authentication to the relay.

#### Raw Install

```bash
git clone https://gitlab.com/rweda/stackdriver-gitlab-ci.git /usr/ci-relay
cd /usr/ci-relay
npm install --only=production
```

##### Example Usage (Raw Install)

```bash
/usr/ci-relay/bin/relay --url "http://localhost:9999/metrics"
```

### Usage

The Relay will attempt to connect to a GitLab CI Runner instance to collect metrics.  Provide `--url` to configure what
Runner the relay should listen to.

Once the Relay gets information from the GitLab CI Runner, it attempts to update [Stackdriver][] with the new data.
Provide a Google Cloud Platform project ID via `--id`, and the name of a Stackdriver metric via `--metric`.  Labels can
be added via `--label`.

The Relay will need to be authenticated with [Stackdriver][] before data is accepted.  Google Compute Engine machines
are authenticated without any configuration, but a [Service Account Key][] can be used in other situations.
By default, the Relay looks for an authentication key under `/var/ci-relay/stackdriver.json`.  An alternative location
can be specified via `--auth-file`.

Start `relay` with the `--help` flag for all available options.

![Relay Help][]

[Stackdriver]: https://cloud.google.com/stackdriver/
[runner-monitoring]: https://docs.gitlab.com/runner/monitoring/README.html
[Service Account Key]: https://cloud.google.com/iam/docs/service-accounts#service_account_keys
[Relay Help]: https://rweda.gitlab.io/stackdriver-gitlab-ci/docs/relay-help.png
