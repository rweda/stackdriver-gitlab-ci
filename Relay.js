const request = require("request-promise");
const RequestError = require("request-promise/errors").RequestError;

/**
 * Queries GitLab CI Runners for the current number of running jobs, and relays that number to Google Stackdriver as a
 * metric.
 * @example <caption>Setting up a new Relay</caption>
 * let relay = new Relay({
 *   url: "http://localhost:9999/metrics", // Connect to GitLab Runner exporting metrics on `:9999`
 *   projectID: "my-great-project", // The Google Cloud Platform project
 *   labels: { gitlabVersion: "9.0" }, // Extra key/value pairs to attach to the datapoint
 * });
 * relay.start();
*/
class Relay {

  /**
   * @param {Object} opts options to configure the status relay.
   * @param {String} opts.url the URL for a GitLab CI Runner's metrics page to query.
   * @param {String} opts.projectID the Google Project ID to submit metrics under.
   * @param {String} [opts.type] the type of resource being submitted.  If `"gce_instance"`, then data will be submitted
   *   as a [Monitored Resource](https://cloud.google.com/monitoring/api/v3/metrics#monitored_resources), with `zone`
   *   and `instance_id` queried from Google Cloud Platform.  `"global"` (default) will submit data under a generic
   *   resource, with the `project_id` given as a label.
   * @param {Boolean} [opts.noAuthFile] if running on Google Compute Engine, set to `true` to disable attempts to read
   *   the auth file, as the library will automatically be authenticated.  Defaults to `false`.
   * @param {String} [opts.authFile] the path to a Google Cloud Platform key file.  If you're running on Google Compute
   *   Engine or authenticated via `gcloud auth application-default login`, then this can be `null`.
   * @param {String} [opts.metric] the name of the metric to submit data as.  Defaults to `"gitlab-ci-runner/builds"`.
   * @param {Object} [opts.labels] labels to send with the metric.  Defaults to an empty object.
   * @param {Integer} [opts.interval] the frequency to update Stackdriver, in milliseconds.  Defaults to `60 * 1000`, or
   *   once every minute.  Stackdriver
   *   [suggests](https://cloud.google.com/monitoring/custom-metrics/creating-metrics#writing-ts) that updates are added
   *   no faster than once per minute.
   * @param {Boolean} [opts.skipClient] if `true`, the Google Cloud Monitoring client won't be initialized.  Useful for
   *   testing to reduce log output when not using the Google API.  Defaults to `false`.
   * @param {Integer} [opts.verbosity] how much Relay should output status to the log.  `0` to silence.  Defaults to
   *   `1`.
  */
  constructor(opts) {
    this.url = opts.url;
    this.projectID = opts.projectID;
    this.noAuthFile = opts.noAuthFile;
    this.authFile = opts.authFile;
    this.type = opts.type || "global";
    this.metric = opts.metric || "gitlab-ci-runner/builds";
    this.labels = opts.labels || {};
    this.interval = opts.interval || 60 * 1000;
    this.verbosity = typeof opts.verbosity !== "number" ? 1 : opts.verbosity;
    if(!opts.skipClient) {
      this.createClient();
    }
  }

  /**
   * Create a new Google Cloud Monitoring client.
   * @return {Object} the created client.
  */
  createClient() {
    const opts = this.noAuthFile ? {} : { projectId: this.projectID, keyFilename: this.authFile };
    return this.client = require("@google-cloud/monitoring")
      .v3(opts)
      .metricServiceClient();
  }

  /**
   * Formats the full metric type description.
   * @return {String} the full metric type description
  */
  get metricType() { return `custom.googleapis.com/${this.metric}`; }

  /**
   * Starts polling for new data.
   * @return {Relay} for chaining
  */
  start() {
    if(this.verbosity >= 1) {
      console.log(`Fetching new data, and setting timer to fetch data every ${Math.floor(this.interval / 1000)}s.`);
    }
    this.queryMetrics();
    this._interval = setInterval(() => this.queryMetrics(), this.interval);
    return this;
  }

  /**
   * Stops polling for new data.
   * @return {Relay} for chaining
  */
  stop() {
    if(this._interval) { clearInterval(this._interval); }
    return this;
  }

  /**
   * Queries the GitLab runner for new metrics over HTTP, parses the value out of the response, then forwards the data
   * to Stackdriver.
   * @return {Promise} resolves when the data has been stored in Stackdriver.
   * @todo Unit test.
  */
  queryMetrics() {
    return this.request()
      .then(data => this.parseMetrics(data))
      .then(val => this.logData(val))
      .then(() => {
        if(this._lastRequestError) {
          if(this.verbosity >= 1) { console.log("Requests are now working!"); }
          this._lastRequestError = null;
        }
      })
      .catch(RequestError, err => {
        if(!this._lastRequestError) {
          console.error("Request failed:");
          console.error(err.stack);
          this._lastRequestError = Date.now();
        }
        else if(this._lastRequestError + (this.interval * 20) > Date.now()) {
          if(this.verbosity >= 1) { console.log("Requests are still failing."); }
        }
        return true;
      })
      .catch(err => {
        console.error("Uncaught error while querying metrics:");
        console.error(err.stack ? err.stack : err);
      });
  }

  /**
   * Request fresh data from the GitLab CI Runner.
   * @return {Promise<String>} resolves to the metrics data returned by the runner.  See
   *   [GitLab Runner monitoring]{@link https://docs.gitlab.com/runner/monitoring/README.html}
   *   for more details about the metrics returned by the runner.
   * @example <caption>Requesting data</caption>
   * relay
   *   .request()
   *   .then(data => console.log(data));
   *   // # HELP ci_docker_machines The total number of machines created.
   *   // # TYPE ci_docker_machines counter
   *   // ci_docker_machines{type="created"} 0
   *   // ci_docker_machines{type="removed"} 0
   *   // ...
  */
  request() {
    return request(this.url);
  }

  /**
   * Get the Google Compute Engine Instance ID for the current machine.
   * @return {Promise<String>} the requested instance ID.
  */
  getInstanceID() {
    return request({
      url: "http://metadata.google.internal/computeMetadata/v1/instance/id",
      headers: { "Metadata-Flavor": "Google" },
    });
  }

  /**
   * Get the Google Compute Engine Zone for the current machine.
   * @return {Promise<String>} the requested instance ID.
  */
  getZone() {
    /**
     * `http://metadata.google.internal/computeMetadata/v1/instance/zone` gives `"projects/[PROJECT ID]/zones/[ZONE]"`.
     * After fetching the entire string, a substr is used to isolate just the zone.
    */
    return request({
      url: "http://metadata.google.internal/computeMetadata/v1/instance/zone",
      headers: { "Metadata-Flavor": "Google" },
    })
      .then(zone => zone.substr(zone.lastIndexOf("/") + 1));
  }

  /**
   * Parses Prometheus data to find the number of running builds.
   * Defaults to returning `0` active builds, as Prometheus doesn't output a data line if no jobs are currently running.
   * @param {String} data the Prometheus data dump to parse
   * @return {Integer} the current number of running builds.
   * @example <caption>Parsing data</caption>
   * let running = relay.request(`
   *   ci_docker_machines{type="created"} 0
   *   ci_runner_builds{stage="prepare_script",state="running"} 3
   * `);
   * console.log(running); // => 3
  */
  parseMetrics(data) {
    let search = data.match(/ci_runner_builds.* ([0-9]+)/g);
    if(!search || search.length < 1) { return 0; }
    return search
      .map(res => res.match(/ci_runner_builds.* ([0-9]+)/)[1])
      .map(i => parseInt(i, 10))
      .reduce((a, b) => a + b);
  }

  /**
   * Provides the resource description for a time series.
   * @return {(Object|Promise<Object>)} the resource description
  */
  getResource() {
    let globalResource = { type: "global", labels: { project_id: this.projectID } };
    if(this.type === "gce_instance") {
      return Promise
        .all([ this.getInstanceID(), this.getZone() ])
        .then(([ instance_id, zone ]) => ({
          type: "gce_instance",
          labels: { instance_id, zone },
        }));
    }
    else if(this.type === "global") { return globalResource; }
    else {
      if(this.verbosity >= 1) { console.log(`Unknown resource type ${this.type}.  Defaulting to 'global' type.`); }
      return globalResource;
    }
  }

  /**
   * Submit data to Google Stackdriver as a custom metric.
   * @param {Integer} val the value to submit
   * @param {Integer} [time] the time to insert the data at, in milliseconds.  Defaults to the current time.
   * @return {Promise} resolves when the data has been submitted.
  */
  logData(val, time) {
    const dataPoint = {
      interval: { endTime: { seconds: (time || Date.now()) / 1000 } },
      value: { int64Value: val },
    };
    return Promise
      .resolve(this.getResource())
      .then(resource => ({
        name: this.client.projectPath(this.projectID),
        timeSeries: [ {
          metric: { type: this.metricType, labels: this.labels },
          resource,
          points: [ dataPoint ],
        } ],
      }))
      .then(data => {
        if(this.verbosity >= 1) { console.log("Logging data."); }
        if(this.verbosity >= 2) { console.info(JSON.stringify(data, null, 2)); }
        return this.client.createTimeSeries(data);
      })
      .then(() => {
        if(this.verbosity >= 1) { console.log("Data logged successfully."); }
      });
  }

}

module.exports = Relay;
